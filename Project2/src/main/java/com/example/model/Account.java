package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Account")
public class Account {
	@Id
    @Column(name = "account_number")
    private String accountNumber;
	
	@Column(name="balance")
	private String balance;
	
	@ManyToOne
	@JoinColumn(name="client_FK")
	private ClientUser cHolder;
	
	@ManyToOne
	@JoinColumn(name="type_FK")
	private AccountType tHolder;
	
	@ManyToOne
	@JoinColumn(name="branch_FK")
	private Branch bHolder;
	
	@OneToMany(mappedBy="accHolder", fetch=FetchType.LAZY)
	private List<Transaction> transList = new ArrayList<>();

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Account(String accountNumber, String balance, ClientUser cHolder) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.cHolder = cHolder;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public ClientUser getcHolder() {
		return cHolder;
	}

	public void setcHolder(ClientUser cHolder) {
		this.cHolder = cHolder;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", balance=" + balance + ", cHolder=" + cHolder + "]";
	}
	
	
}
