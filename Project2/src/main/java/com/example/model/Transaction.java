package com.example.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Transaction")
public class Transaction {
	@Id
	@Column(name="transaction_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int transactionId;
	
	
	@Column(name="description")
	private String description;
	
	@Column(name="Transaction_date_time")
	private java.sql.Timestamp  TransactionDateTime;
	
	@Column(name="debit_amount")
	private int debitAmount;
	
	@Column(name="credit_amount")
	private int cebitAmount;
	
	@ManyToOne
	@JoinColumn(name="transaction_FK")
	private Account accHolder;

	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Transaction(int transactionId, String description, Timestamp transactionDateTime, int debitAmount,
			int cebitAmount, Account accHolder) {
		super();
		this.transactionId = transactionId;
		this.description = description;
		TransactionDateTime = transactionDateTime;
		this.debitAmount = debitAmount;
		this.cebitAmount = cebitAmount;
		this.accHolder = accHolder;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public java.sql.Timestamp getTransactionDateTime() {
		return TransactionDateTime;
	}

	public void setTransactionDateTime(java.sql.Timestamp transactionDateTime) {
		TransactionDateTime = transactionDateTime;
	}

	public int getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(int debitAmount) {
		this.debitAmount = debitAmount;
	}

	public int getCebitAmount() {
		return cebitAmount;
	}

	public void setCebitAmount(int cebitAmount) {
		this.cebitAmount = cebitAmount;
	}

	public Account getAccHolder() {
		return accHolder;
	}

	public void setAccHolder(Account accHolder) {
		this.accHolder = accHolder;
	}

	public int getTransactionId() {
		return transactionId;
	}

	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", description=" + description + ", TransactionDateTime="
				+ TransactionDateTime + ", debitAmount=" + debitAmount + ", cebitAmount=" + cebitAmount + ", accHolder="
				+ accHolder + "]";
	}
	
	
}
